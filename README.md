## Change default Java version on macOS 11 BigSur

```sh
javahome() {
  unset JAVA_HOME 
  export JAVA_HOME=$(/usr/libexec/java_home -v "$1");
  java -version
}

alias j1.8='javahome 1.8'
alias j11='javahome 11'
alias j14='javahome 14'
alias j15='javahome 15'
```

`javahome` function and `aliases` in action:

[![asciicast](https://asciinema.org/a/FbWTvVTzbmYGTy79VotdH36yI.svg?t=53)](https://asciinema.org/a/FbWTvVTzbmYGTy79VotdH36yI?t=8)

See also [Change default Java version on macOS 11 BigSur & persist it](https://www.lotharschulz.info/2021/01/11/change-default-java-version-on-macos-11-bigsur-persist-it/) on [lotharschulz.info](https://www.lotharschulz.info/)
